# UnboundPSService

UnboundPSService は、 Unbound を Windows 上にサービスとしてインストールするためのPowershell Script ラッパーです。

## インストール手順

### UnboundPSService のインストール手順

- `C:\Unbound` に配置することを前提としています。

1. **ソースコードのダウンロードと展開**
   
   GitLabリポジトリからUnboundPSServiceのソースコードをダウンロードし、`C:\Unbound`に展開します。

   - リポジトリURL: [https://gitlab.com/kamiyonanayo/unbound-psservice](https://gitlab.com/kamiyonanayo/unbound-psservice)
   - ダウンロードリンク: [https://gitlab.com/kamiyonanayo/unbound-psservice/-/archive/main/unbound-psservice-main.zip](https://gitlab.com/kamiyonanayo/unbound-psservice/-/archive/main/unbound-psservice-main.zip)

2. **Unboundの実行ファイルのダウンロード**
   
   Unboundの公式ウェブサイト([https://nlnetlabs.nl/projects/unbound/](https://nlnetlabs.nl/projects/unbound/))からWindows用の実行ファイル(no install版)をダウンロードし、同じフォルダの(`C:\Unbound\unbound`)に展開します。

3. **Unboundの設定ファイルの修正**

   `Unbound\unbound\service.conf` を修正して動作設定を記述します。

### インストール後のディレクトリ構成

インストール後、`C:\Unbound` ディレクトリの構成は以下のようになります

```
C:\Unbound\
|
|-- UnboundPSService.ps1
|-- README.md
|-- Start-UnboundProcess.ps1
|
+---unbound
|   |-- service.conf
|   |-- unbound.exe
|   |-- unbound-anchor.exe
|   |-- unbound-checkconf.exe
|   |-- unbound-control-setup.cmd
|   |-- unbound-control.exe
|   |-- unbound-host.exe
|   |-- unbound-service-install.exe
|   |-- unbound-service-remove.exe
|   |-- unbound-website.url
|   \-- ....
|
\---logs
    \-- .gitignore
```

## UnboundPSService の使用方法

UnboundPSServiceを利用してUnboundをWindowsサービスとして操作するための方法です。これらの操作は管理者権限を持つPowerShellで実行する必要があります。

### サービスの登録

WindowsサービスとしてUnboundを登録するには、以下のコマンドを実行します。サービスを実行するユーザーとしては、最小の権限で問題ないため `Local Service` を指定します。

```ps1
./UnboundPSService.ps1 -Setup -UserName LocalService -Verbose
./UnboundPSService.ps1 -Status
```
**Windowsサービスに登録するとコントロールパネルのサービスから自動起動の設定ができるようになります。**

### サービスの起動

Unboundサービスを起動するためには、以下のコマンドを使用します。

```ps1
./UnboundPSService.ps1 -Start -Verbose
./UnboundPSService.ps1 -Status
```

### サービスの停止

Unboundサービスを停止するには、次のコマンドを実行します。

```ps1
./UnboundPSService.ps1 -Stop -Verbose
./UnboundPSService.ps1 -Status
```

### サービスの再起動

Unboundサービスを再起動する場合は、以下のコマンドを使用します。

```ps1
./UnboundPSService.ps1 -Restart -Verbose
./UnboundPSService.ps1 -Status
```

### サービスの削除

WindowsサービスからUnboundを削除するには、次のコマンドを実行します。

```ps1
./UnboundPSService.ps1 -Remove -Verbose
./UnboundPSService.ps1 -Status
```

## WindowsでUnboundサービスのDNS設定変更方法

UnboundをWindowsサービスとして利用している場合、システムのDNS設定を変更してUnboundを参照するように設定する必要があります。以下の手順で設定を変更します。

### DNS設定の変更手順

1. **ネットワーク設定へのアクセス**

   - 「設定」を開き、「ネットワークとインターネット」に進みます。
   - 「ネットワークの詳細設定」を選択します。
   - 「その他のアダプターのオプション」の編集ボタンをクリックします。

2. **IPv4のDNS設定**

   - ネットワーク接続のリストから接続を選択し、「プロパティ」を開きます。
   - 「インターネット プロトコル バージョン 4 (TCP/IPv4)」を選択し、「プロパティ」をクリックします。
   - 「優先DNSサーバ」に `127.0.0.1` を入力し、代替DNSサーバは空のままにします。

3. **IPv6のDNS設定**

   - 同様に、「インターネット プロトコル バージョン 6 (TCP/IPv6)」を選択し、「プロパティ」をクリックします。
   - 「優先DNSサーバ」に `::1` を入力し、代替DNSサーバは空のままにします。

## Unboundの設定ファイルのカスタマイズ方法

`Start-UnboundProcess.ps1` スクリプトを修正することで、ネットワーク環境に応じてUnboundの設定ファイルを切り替えることができます。以下に例を示します。

### 特定のネットワークに接続できる場合の設定ファイル切り替え

#### 1. Test-Connectionを使用した例

```ps1
[CmdletBinding()]
[OutputType([System.Diagnostics.Process])]
Param()

$conf = "conf/service.conf"

if (Test-Connection 192.168.1.1 -TimeoutSeconds 1 -Count 1 -Quiet) {
   $conf = "conf/service_other.conf"
}

$UnboundArgumentList = @(
    "-c", $conf,
    "-d"
)

$Arguments = @{
    WindowStyle="Hidden"
    PassThru = $true
    FilePath="$PSScriptRoot/unbound/unbound.exe"
    WorkingDirectory="$PSScriptRoot/unbound"
    ArgumentList=@($UnboundArgumentList)
    RedirectStandardOutput="$PSScriptRoot/logs/stdout.txt"
    RedirectStandardError="$PSScriptRoot/logs/stderr.txt"
}

Start-Process @Arguments
```

#### 2. Pingを使用した例

```ps1
[CmdletBinding()]
[OutputType([System.Diagnostics.Process])]
Param()

$conf = "conf/service.conf"

$null = ping -n 1 -w 100 192.168.1.1
if ($LastExitCode -eq 0) {
   $conf = "conf/service_other.conf"
}

# 省略

```

#### 3. 特定のSSIDのWifiを使用している場合の例

```ps1
[CmdletBinding()]
[OutputType([System.Diagnostics.Process])]
Param()

$conf = "conf/Corefile"

if ("foo_var_SSID" -eq (Get-NetConnectionProfile).Name) {
   $conf = "conf/service_other.conf"
}

# 省略

```
