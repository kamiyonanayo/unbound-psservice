[CmdletBinding()]
[OutputType([System.Diagnostics.Process])]
Param()

$conf = "$PSScriptRoot\unbound\service.conf"

$UnboundArgumentList = @(
    "-c", $conf,
    "-d"
)

$Arguments = @{
    WindowStyle="Hidden"
    PassThru = $true
    FilePath="$PSScriptRoot/unbound/unbound.exe"
    WorkingDirectory="$PSScriptRoot/unbound"
    ArgumentList=@($UnboundArgumentList)
    RedirectStandardOutput="$PSScriptRoot/logs/stdout.txt"
    RedirectStandardError="$PSScriptRoot/logs/stderr.txt"
}

Start-Process @Arguments
