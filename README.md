# UnboundPSService

UnboundPSService is a Powershell Script wrapper for installing Unbound as a service on Windows.

## Installation Instructions

### UnboundPSService Installation Procedure

- Assumes deployment in `C:\Unbound`.

1. **Downloading and Extracting the Source Code**
   
   Download the UnboundPSService source code from the GitLab repository and extract it into `C:\Unbound`.

   - Repository URL: [https://gitlab.com/kamiyonanayo/unbound-psservice](https://gitlab.com/kamiyonanayo/unbound-psservice)
   - Download Link: [https://gitlab.com/kamiyonanayo/unbound-psservice/-/archive/main/unbound-psservice-main.zip](https://gitlab.com/kamiyonanayo/unbound-psservice/-/archive/main/unbound-psservice-main.zip)

2. **Downloading Unbound Executable**
   
   Download the Windows executable (no install version) of Unbound from the official website ([https://nlnetlabs.nl/projects/unbound/](https://nlnetlabs.nl/projects/unbound/)) and extract it into the same folder (`C:\Unbound\unbound`).

3. **Modifying Unbound Configuration File**

   Edit `Unbound\unbound\service.conf` to set up the operational configuration.

### Directory Structure After Installation

After installation, the structure of the `C:\Unbound` directory will be as follows:

```
C:\Unbound\
|
|-- UnboundPSService.ps1
|-- README.md
|-- Start-UnboundProcess.ps1
|
+---unbound
|   |-- service.conf
|   |-- unbound.exe
|   |-- unbound-anchor.exe
|   |-- unbound-checkconf.exe
|   |-- unbound-control-setup.cmd
|   |-- unbound-control.exe
|   |-- unbound-host.exe
|   |-- unbound-service-install.exe
|   |-- unbound-service-remove.exe
|   |-- unbound-website.url
|   \-- ....
|
\---logs
    \-- .gitignore
```

## How to Use UnboundPSService

Methods for operating Unbound as a Windows service using UnboundPSService. These operations need to be executed in PowerShell with administrative privileges.

### Registering the Service

To register Unbound as a Windows service, execute the following commands. The user running the service can have minimal privileges, so specify `Local Service`.

```ps1
./UnboundPSService.ps1 -Setup -UserName LocalService -Verbose
./UnboundPSService.ps1 -Status
```
**Once registered as a Windows service, you can set it to start automatically from the Control Panel's services.**

### Starting the Service

To start the Unbound service, use the following commands.

```ps1
./UnboundPSService.ps1 -Start -Verbose
./UnboundPSService.ps1 -Status
```

### Stopping the Service

To stop the Unbound service, execute the following command.

```ps1
./UnboundPSService.ps1 -Stop -Verbose
./UnboundPSService.ps1 -Status
```

### Restarting the Service

To restart the Unbound service, use the following commands.

```ps1
./UnboundPSService.ps1 -Restart -Verbose
./UnboundPSService.ps1 -Status
```

### Removing the Service

To remove Unbound from Windows services, execute the following commands.

```ps1
./UnboundPSService.ps1 -Remove -Verbose
./UnboundPSService.ps1 -Status
```

## How to Change DNS Settings for Unbound Service on Windows

When using Unbound as a Windows service, you need to change the system's DNS settings to refer to Unbound. Follow the steps below to modify the settings.

### Changing DNS Settings Procedure

1. **Accessing Network Settings**

   - Open 'Settings', go to 'Network & Internet'.
   - Select 'Advanced network settings'.
   - Click the edit button for 'More adapter options'.

2. **DNS Setting for IPv4**

   - Select a connection from the list of network connections and open 'Properties'.
   - Choose 'Internet Protocol Version 4 (TCP/IPv4)' and click 'Properties'.
   - Enter `127.0.0.1` in 'Preferred DNS server', leaving the alternate DNS server blank.

3. **DNS Setting for IPv6**

   - Similarly, select 'Internet Protocol Version 6 (TCP/IPv6)' and click 'Properties'.
   - Enter `::1` in 'Preferred DNS server', leaving the alternate DNS server blank.

## Customizing Unbound Configuration Files

You can switch Unbound configuration files according to the network environment by modifying the `Start-UnboundProcess.ps1` script. Here are some examples:

### Switching

 Configuration Files When Connected to a Specific Network

#### 1. Example Using Test-Connection

```ps1
[CmdletBinding()]
[OutputType([System.Diagnostics.Process])]
Param()

$conf = "conf/service.conf"

if (Test-Connection 192.168.1.1 -TimeoutSeconds 1 -Count 1 -Quiet) {
   $conf = "conf/service_other.conf"
}

$UnboundArgumentList = @(
    "-c", $conf,
    "-d"
)

$Arguments = @{
    WindowStyle="Hidden"
    PassThru = $true
    FilePath="$PSScriptRoot/unbound/unbound.exe"
    WorkingDirectory="$PSScriptRoot/unbound"
    ArgumentList=@($UnboundArgumentList)
    RedirectStandardOutput="$PSScriptRoot/logs/stdout.txt"
    RedirectStandardError="$PSScriptRoot/logs/stderr.txt"
}

Start-Process @Arguments
```

#### 2. Example Using Ping

```ps1
[CmdletBinding()]
[OutputType([System.Diagnostics.Process])]
Param()

$conf = "conf/service.conf"

$null = ping -n 1 -w 100 192.168.1.1
if ($LastExitCode -eq 0) {
   $conf = "conf/service_other.conf"
}

# Omitted for brevity
```

#### 3. Example for Specific SSID Wifi

```ps1
[CmdletBinding()]
[OutputType([System.Diagnostics.Process])]
Param()

$conf = "conf/Corefile"

if ("foo_var_SSID" -eq (Get-NetConnectionProfile).Name) {
   $conf = "conf/service_other.conf"
}

# Omitted for brevity
```
